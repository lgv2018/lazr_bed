# lazr_bed alternate belt path
For those that have concerns about the beam hitting the belt with the original belt path I designed this which should help.
We've been running the original design at our makerspace for about 1 year now and the belt hasn't broken and barely looks singed, so I think the hype is over dramatize, but here we are.

I've attached a picture of what the new belt path looks like, with a tensioning system outside the bottom frame.

This design has not been printed or tested, so if you're feeling adventurous, please feel free to report back here with your findings!

![alternate_belt_path](lazr_bed_alternate_belt_path.png))