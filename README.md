# lazr_bed
![laser bed render](img/lazr_bed_render.png)

lazr_bed is an Open Source, motorized Z table for laser cutters. Designed specifically for K40 Chinese lasers.

# Overview

The project was originally designed to be used in our chinese K40 laser cutter at [River City Labs Makerspace](https://rivercitylabs.space/) in Peoria, IL.

This project aims to accomplish the following:
1. The dimensions of the bed (length, width, height) should be easily scaled to accomodate any bed size needed.
2. All parts to be easily sourced using commodity hardware and complex parts to be 3D Printed.
3. No additional tools needed other than a hex wrench.

# STL's
Ready to print STL's are available for purchase on the [Cults3D page](https://cults3d.com/en/3d-model/tool/lazr_bed-open-source-z-table). You may download the fusion archive here and export your own STL's if you wish.

# BOM
| Component                            | Qty |
|--------------------------------------|-----|
| bearing - 608                        | 8   |
| belt - gt2 - 1220mm                  | 1   |
| eclip - 8mm                          | 4   |
| extrusion - 15x15x255                | 5   |
| extrusion - 15x15x320                | 1   |
| extrusion - 15x15x350                | 4   |
| leadscrew - T8 - 95mm                | 4   |
| microswitch                          | 1   |
| nut - m2 (microswitch)               | 2   |
| nut - m3 - (15x15 extrusion profile) | 56  |
| nut - T8 - brass (small profile)     | 4   |
| print - limit switch mount           | 1   |
| print - lower corner                 | 4   |
| print - motor mount                  | 1   |
| print - right angle bracket          | 4   |
| print - upper corner                 | 4   |
| pulley - gt2 - 20t - 5mm bore        | 1   |
| pulley - gt2 - 40t - 8mm bore        | 4   |
| pulley - gt2 - idler - 4mm bore      | 4   |
| screw - m2 - 14mm (microswitch)      | 2   |
| screw - m3 - 8mm                     | 64  |
| shoulder bolt - m3 - 10mm            | 4   |
| standoff - m3 - 10mm                 | 4   |
| stepper motor - nema 17              | 1   |
| washer - m3 - 9mm (idlers)           | 8   |

# Assembly
Assembly is pretty straightforward. Instructions will come soon when I have the time to write them up, but I recommend going through the fusion 360 archive to see where all the parts go. The only hidden parts are the 608 bearings and tnuts.

I recommend assembling the extrusion pieces on a very flat surface, I used a glass coffee table.

Tnuts will go wherever a screw enters the extrusion (screws, shoulderbolts)

All idler pulleys get a shoulderbolt and a washer.

Two 608 bearings are press fit into each round recess in the bottom corner pieces.

I recommend assembling the top frame with the printed pieces first, then adding the brass nuts last.

The top I used is expanded, galvanized wire mesh from a big box store. It's cut to size and ziptied to the top around the extrusions. At this size it's very sturdy and allows a lot of air through. The galvanization shouldn't be a big deal since the laser should be vented anyway.

I also found it necessary to grind off the factory bevel from the lead screws to reduce the stickout on the bottom of the bed. The e-clips only sit on the last full thread, so I added a couple drops of super glue where the e-clips meet the leadscrews and they've been holding perfectly.

here are what the leadscrews and e-clips look like:
![leadscrews](img/leadscrews.jpg)

Here is the table fully assembled for reference:
![fully assembled lazr_bed](img/fully_assembled.jpg)

Top with reference belt path:
![top view of the bed](img/top.jpg)

Bottom view:
![bottom view of the bed](img/bottom.jpg)

# Design Reasoning
I originally designed to use 40 tooth pulleys, then tried to assemble with 20 tooth to save a few bucks, but the belt path was too stiff and the stepper couldn't handle it. Going back to 40t gave it a 2:1 reduction and works nicely.

The bed design is meant to maximize Z height while avoiding any contact with a laser head with an air nozzle on. This means the 95mm leadscrew length is fairly strict. This gives the limit switch enough gap to use to home on top of your material (to auto focus based on material thickness). The limit switch holder is the main height constraint without hitting the laser head.

I used extrusion because it's easy to source at specific lengths, eliminating the need for accurate cutting by the user.

The bottom corners have the large flat pieces to help offload the load onto the extrusion and not entirely on the screws. Also it helps provide the clearance for the eclip.

# Future considerations
As soon as I was "done" building this bed, I already wanted to make it better. 

Here are a few things I'd like to see improved on this design:

1. Better limit switch holder. This one is very rudimentary and doesn't have a max limit switch.
2. Smooth Rods. This design can wobble a bit, which shouldn't matter unless you're changing height mid job. I'd like this design to be able to do mid job adjustments to account for depth of cut on multiple passes.
3. Eliminate bed shifting. Currently the bed can shift a bit in the laser as it's just smooth plastic on steel. I was thinking of adding some recessed magnets to the bottom corners or something.